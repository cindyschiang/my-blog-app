from django.urls import path

from posts.views import CreatePostView, list_all_posts, show_post_detail

urlpatterns = [
    path("<int:pk>/", show_post_detail, name="post_detail"),
    path("", list_all_posts, name="post_list"),
    path("create/", CreatePostView.as_view(), name="create_post")
]
