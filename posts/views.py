from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView

from .forms import PostForm
from .models import Post


# Create your views here.
def show_post_detail(request, pk):
    post = Post.objects.get(pk=pk)
    context = {"post": post}
    return render(request, "posts/detail.html", context)


def list_all_posts(request):
    # 1.  get the data
    #  - use the django model methods to get the data
    posts = Post.objects.all()

    # 2. pass the data to the template
    # make a context dictionary with "posts" as the key and all the posts from
    # the database as the value
    context = {"posts": posts}

    # 3. return the template to the browser
    # return an http response by returning the django render function,
    # which calls the template and gives it the data in the context
    return render(request, "posts/list.html", context)


def create_post(request):
    # 1. make a variable named form
    #   a. validate form, etc
    #   b. save form
    # 2. once form is saved, return something: redirect to lists_all_posts
    # 3. create our context with key "form" and value of the form we made

    form = PostForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("post_list")
    context = {"form": form}
    return render(request, "posts/create.html", context)


class CreatePostView(CreateView):
    model = Post
    template_name = "posts/create.html"
    fields = [
        "title",
        "content"
    ]
    success_url = "/"
